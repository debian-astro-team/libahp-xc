#include <stdint.h>
#include <stdio.h>
#include <ahp/ahp_xc.h>

int main(int argc, char** argv)
{
 uint64_t threadCount = 0;
 int rc=0;

 threadCount=ahp_xc_max_threads(threadCount);
 if (threadCount!=1) {
   fprintf(stderr,"D: initial threadCount (%li) != 1\n",threadCount);
   rc=1;
 }

 if (rc==0) {
   printf("D: PASS\n");
 }

 return rc;
}

